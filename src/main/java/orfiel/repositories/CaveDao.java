package orfiel.repositories;

import orfiel.model.Cave;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class CaveDao {

    public Cave cAdd(Cave cave) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction val = em.getTransaction();
        val.begin();
        em.persist(cave);
        val.commit();
        em.close();
        return cave;
    }

    public List<Cave> findAllCaves() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Cave> query = cb.createQuery(Cave.class);
        query.from(Cave.class);
        List<Cave> caves = em.createQuery(query).getResultList();
        em.close();
        return caves;
    }
}
