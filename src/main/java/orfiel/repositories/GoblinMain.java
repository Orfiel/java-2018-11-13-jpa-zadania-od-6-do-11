package orfiel.repositories;

import orfiel.model.Cave;
import orfiel.model.Goblin;
import orfiel.model.Weapon;

public class GoblinMain {

    public static void main(String[] args) {


        GoblinDao goblinDao = new GoblinDao();
        CaveDao caveDao = new CaveDao();
        WeaponDao weaponDao = new WeaponDao();

        Cave nora = new Cave(1800);
        Cave korzonek = new Cave(2500);
        Cave krypta = new Cave(3654);//pusta


        Goblin psysiu = new Goblin("Psysiu", 180, nora);
        Goblin kerkeesi = new Goblin("Kerkeesi", 150, korzonek);
        Goblin mekkaflow = new Goblin("Mekkaflow", 137, nora);
        Goblin quietstrip = new Goblin("Quietstrip", 123, korzonek);
        Goblin ginbifunk = new Goblin("Ginbifunk", 142, nora);
        Goblin sisli = new Goblin("Sisli", 180, korzonek);
        Goblin klufelbuck = new Goblin("Klufelbuck", 97, nora);
        Goblin jukol = new Goblin("Jukol", 79);
        Goblin trickybrick = new Goblin("Trickybrick", 121);
        Weapon rage = new Weapon(Weapon.Wound.BLUNT, "Rage", 350);


        Weapon malice = new Weapon(Weapon.Wound.STAB, "Malice", 130);
        Weapon solitude = new Weapon(Weapon.Wound.STAB, "Solitude", 40);
        Weapon demonLord = new Weapon(Weapon.Wound.BLUNT, "DemonLord", 666);
        Weapon bFS = new Weapon(Weapon.Wound.SLASHING, "BFS", 320);
        Weapon bloodsurge = new Weapon(Weapon.Wound.SLASHING, "Bloodsurge", 289);
        Weapon antique = new Weapon(Weapon.Wound.BLUNT, "Antique", 489);//bez goblina
        Weapon trauma = new Weapon(Weapon.Wound.SLASHING, "Trauma", 500);//bez goblina

        caveDao.cAdd(nora);
        caveDao.cAdd(korzonek);
        caveDao.cAdd(krypta);
        goblinDao.create(psysiu);
        weaponDao.wAdd(rage);


        goblinDao.create(kerkeesi);
        goblinDao.create(mekkaflow);
        goblinDao.create(quietstrip);
        goblinDao.create(ginbifunk);
        goblinDao.create(sisli);
        goblinDao.create(klufelbuck);
        goblinDao.create(jukol);//bez broni
        goblinDao.create(trickybrick);//bez broni


        weaponDao.wAdd(malice);
        weaponDao.wAdd(solitude);
        weaponDao.wAdd(demonLord);
        weaponDao.wAdd(bFS);
        weaponDao.wAdd(bloodsurge);
        weaponDao.wAdd(antique);
        weaponDao.wAdd(trauma);

        caveDao.findAllCaves().forEach(System.out::println);
        goblinDao.findAllGoblins().forEach(System.out::println);
        weaponDao.findAllWeapons().forEach(System.out::println);

        GoblinEntityManagerFactory.close();

    }
}
