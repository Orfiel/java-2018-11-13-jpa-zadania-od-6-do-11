package orfiel.repositories;


import orfiel.model.Weapon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class WeaponDao {


    public Weapon wAdd(Weapon weapon) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction val = em.getTransaction();
        val.begin();
        em.persist(weapon);
        val.commit();
        em.close();
        return weapon;
    }

    public List<Weapon> findAllWeapons() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        List<Weapon> weapons = em.createQuery("select w from Weapon w").getResultList();
        em.close();
        return weapons;
    }
}
