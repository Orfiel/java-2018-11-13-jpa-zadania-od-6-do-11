package orfiel.repositories;

import orfiel.model.Goblin;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;


public class GoblinDao {


    public Goblin create(Goblin goblin) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction val = em.getTransaction();
        val.begin();
        em.persist(goblin);
        val.commit();
        em.close();
        return goblin;
    }

    public void delete(Goblin goblin) {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        EntityTransaction val = em.getTransaction();
        val.begin();
        em.remove(em.merge(goblin));
        val.commit();
        em.close();
    }

    public List<Goblin> findAllGoblins() {
        EntityManager em = GoblinEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Goblin> query = cb.createQuery(Goblin.class);
        query.from(Goblin.class);
        List<Goblin> goblins = em.createQuery(query).getResultList();
        em.close();
        return goblins;

    }
}


