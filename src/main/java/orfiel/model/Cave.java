package orfiel.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "caves")
public class Cave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int surface;

    @OneToMany(mappedBy = "cave", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Goblin> goblins = new ArrayList<>();


    public Cave() {
    }

    public Cave(int surface) {
        this.surface = surface;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public List<Goblin> getGoblins() {
        return goblins;
    }

    public void setGoblins(List<Goblin> goblins) {
        this.goblins = goblins;
    }

    @Override
    public String toString() {
        return "Cave{" +
                "id=" + id +
                ", surface=" + surface +
                ", goblins=" + (goblins != null ? goblins.stream().map(Goblin::getName).collect(Collectors.joining()) : null) +
                '}';
    }
}
