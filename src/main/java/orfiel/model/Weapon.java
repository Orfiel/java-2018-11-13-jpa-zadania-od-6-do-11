package orfiel.model;

import javax.persistence.*;

@Entity
@Table(name = "weapons")
public class Weapon {

    public enum Wound {
        STAB,
        SLASHING,
        BLUNT;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column
    private Wound wound;

    @Column
    private String name;

    @Column
    private int damage;

    @OneToOne(cascade = CascadeType.ALL)
    private Goblin owner;

    public Weapon() {
    }

    public Weapon(Wound wound, String name, int damage) {
        this.wound = wound;
        this.name = name;
        this.damage = damage;
    }

    public Weapon(Wound wound, String name, int damage, Goblin owner) {
        this.wound = wound;
        this.name = name;
        this.damage = damage;
        this.owner = owner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Wound getWound() {
        return wound;
    }

    public void setWound(Wound wound) {
        this.wound = wound;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public Goblin getOwner() {
        return owner;
    }

    public void setOwner(Goblin owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "id=" + id +
                ", wound=" + wound +
                ", name='" + name + '\'' +
                ", damage=" + damage +
                ", owner=" + (owner != null ? owner.getName() : null) +
                '}';
    }
}
