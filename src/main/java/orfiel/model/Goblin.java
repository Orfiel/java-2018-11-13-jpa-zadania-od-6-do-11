package orfiel.model;

import javax.persistence.*;

@Entity
@Table(name = "goblins")
public class Goblin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int height;

    @ManyToOne
    @JoinColumn(name = "cave", referencedColumnName = "id")
    private Cave cave;

    @OneToOne(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
    private Weapon weapon;

    public Goblin() {
    }

    public Goblin(String name, int height) {
        this.name = name;
        this.height = height;
    }

    public Goblin(String name, int height, Cave cave) {
        this.name = name;
        this.height = height;
        this.cave = cave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Cave getCave() {
        return cave;
    }

    public void setCave(Cave cave) {
        this.cave = cave;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    @Override
    public String toString() {
        return "Goblin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", cave=" + (cave != null ? cave.getId() : null) +
                ", weapon=" + (weapon != null ? weapon.getName() : null) +
                '}';
    }
}
